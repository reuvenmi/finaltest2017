<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property string $category
 * @property string $author
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $created_by
 * @property string $updated_by
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
			['body','string'],
            [['created_at', 'updated_at'], 'integer'],
            [['title', 'category', 'author', 'status', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category' => 'Category',
            'author' => 'Author',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
		public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);
		
		//new record save to all else save only to updated fields
        if ($this->isNewRecord)
		{
			$this->created_at = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd h:i:s');
			$this->created_by = Yii::$app->user->identity->id;
			$this->updated_at = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd h:i:s');
			$this->updated_by = Yii::$app->user->identity->id;
			$this->author = Yii::$app->user->identity->id;
		}else{
			$this->updated_at = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd h:i:s');
			$this->updated_by = Yii::$app->user->identity->id;
		}
		
		

        return $return;
    }
	public function getFindCategory()
    {
		return  Category::findOne($this->category);
    }
	
	public function getFindStatus()
    {
		return  Status::findOne($this->status	);
    }	
}
